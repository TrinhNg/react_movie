import { BrowserRouter, Switch, Route } from "react-router-dom";
import "./App.css";
import MovieNew from "./views/MovieListine";
import MovieList from "./views/MovieNew";

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/movie/new" component={MovieNew} />
        <Route path="/" component={MovieList} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
