import { Button, FormControlLabel, Switch, TextField } from "@material-ui/core";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import "date-fns";
import DateFnsUtils from "@date-io/date-fns";
import React from "react";
import useStyle from "../MovieNew/style";
import { useFormik } from "formik";
import axios from "axios";
import { format } from "date-fns";

const MovieNew = () => {
  const classes = useStyle();
  const { container, formControl } = classes;
  const formik = useFormik({
    initialValues: {
      maPhim: "",
      tenPhim: "",
      trailer: "",
      moTa: "",
      maNhom: "GP01",
      //    nếu không chọn mặc định là ngày khởi chiếu
      ngayKhoiChieu: new Date(),
      sapChieu: false,
      dangChieu: false,
      hot: false,
      danhGia: "",
      hinhAnh: "",
    },
  });
  //   hàm sử lý ngày tháng
  const handelChangeDate = (date) => {
    // cập nhật lại giá trị ngày khởi chiếu của formik
    formik.setFieldValue("ngayKhoiChieu", date);
  };
  //  hàm sử lý Switch với giá trị boolen
  const handleChangeSwitch = (e) => {
    formik.setFieldValue(e.target.name, e.target.checked);
  };
  // hàm sử lý hình ảnh, files là mảng nên chọn hình chọn thứ 0
  const handleChangeFile = (e) => {
    formik.setFieldValue(e.target.name, e.target.files[0]);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    // tạo biến ngày mới
    const newMovie = {
      ...formik.values,
      ngayKhoiChieu: format(formik.values.ngayKhoiChieu, "dd/MM/yyyy"),
      //   ngayKhoiChieu: `${formik.values.ngayKhoiChieu.getDate()}/${
      //     formik.values.ngayKhoiChieu.getMonth() + 1
      //   }/${formik.values.ngayKhoiChieu.getFullYear()}`,
    };
    console.log(formik.values);
    // gửi thông tin form lên api
    const formData = new FormData();
    // đưa obj vào form Dt
    for (let key in newMovie) {
      formData.append(key, formik.values[key]);
    }
    try {
      const res = await axios({
        method: "POST",
        url: "http://movieapi.cyberlearn.vn/api/QuanLyPhim/ThemPhimUploadHinh",
        data: formData,
      });
      console.log(res);
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <div className={container}>
      <h1>Create New Movie</h1>
      <form onSubmit={handleSubmit}>
        <TextField
          name="maPhim"
          onChange={formik.handleChange}
          className={formControl}
          fullWidth
          label="Mã phim"
          variant="outlined"
        />
        <TextField
          name="tenPhim"
          onChange={formik.handleChange}
          className={formControl}
          fullWidth
          label="Tên phim"
          variant="outlined"
        />
        <TextField
          name="trailer"
          onChange={formik.handleChange}
          className={formControl}
          fullWidth
          label="Trailer"
          variant="outlined"
        />
        <TextField
          name="mota"
          onChange={formik.handleChange}
          className={formControl}
          fullWidth
          label="Mô tả"
          variant="outlined"
        />
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <KeyboardDatePicker
            disableToolbar
            variant="inline"
            inputVariant="outlined"
            // cập nhật value lên lại input
            value={formik.values.ngayKhoiChieu}
            onChange={handelChangeDate}
            format="dd/MM//yyyy"
            value={new Date()}
            label="Ngày khởi chiếu !!!"
          />
        </MuiPickersUtilsProvider>
        <div>
          <FormControlLabel
            control={
              <Switch
                color="primary"
                name="dangChieu"
                onChange={handleChangeSwitch}
              />
            }
            label="Đang chiếu"
          />
          <FormControlLabel
            control={
              <Switch
                color="primary"
                name="sapChieu"
                onChange={handleChangeSwitch}
              />
            }
            label="Sắp chiếu"
          />
          <FormControlLabel
            control={
              <Switch
                color="primary"
                name="hot"
                onChange={handleChangeSwitch}
              />
            }
            label="Hot"
          />
        </div>
        <TextField
          name="danhGia"
          onChange={formik.handleChange}
          className={formControl}
          fullWidth
          label="Đánh giá"
          variant="outlined"
        />
        <input
          name="hinhAnh"
          onChange={handleChangeFile}
          className={formControl}
          type="file"
        />
        <Button type="submit" variant="contained" color="primary">
          Submit
        </Button>
      </form>
    </div>
  );
};

export default MovieNew;
